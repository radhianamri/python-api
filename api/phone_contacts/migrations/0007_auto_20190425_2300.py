# Generated by Django 2.2 on 2019-04-25 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_contacts', '0006_auto_20190425_2052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonecontact',
            name='profile_photo',
            field=models.ImageField(null=True, upload_to='images/'),
        ),
    ]
