# Generated by Django 2.2 on 2019-04-25 11:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('phone_contacts', '0002_auto_20190425_1730'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PhoneContacts',
            new_name='PhoneContact',
        ),
        migrations.AlterModelTable(
            name='phonecontact',
            table='phone_contact',
        ),
    ]
