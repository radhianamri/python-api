from django import forms
from .models import PhoneContact


class PhoneContactForm(forms.ModelForm):
    class Meta:
        model = PhoneContact
        fields = ('id', 'contact_name', 'mobile_number', 'email', 'profile_photo')
