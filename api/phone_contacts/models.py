from django.db import models
from django.utils import timezone


class PhoneContact(models.Model):
    contact_name = models.CharField(max_length=30, blank=True)
    mobile_number = models.CharField(max_length=20, blank=True)
    email = models.CharField(max_length=30, blank=True)
    profile_photo = models.ImageField(upload_to='images/', blank=False, null=True)
    created_dt = models.DateTimeField(auto_now_add=True)
    updated_dt = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "phone_contact"

    def __str__(self):
        return self.contact_name