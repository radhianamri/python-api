from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import PhoneContact
from .serializers import PhoneContactSerializer

# tests for views
"""

class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_phone_contact(contact_name="", mobile_number="", email="", profile_photo=""):
        if contact_name != "" and mobile_number != "":
            PhoneContact.objects.create(contact_name=contact_name, mobile_number=mobile_number, email=email, profile_photo=profile_photo)

    def setUp(self):
        # add test data
        self.create_phone_contact("like glue", "sean paul")

class GetAllPhoneContact(BaseViewTest):

    def get_all_phone_contacts(self):
        """
        This test ensures that all songs added in the setUp method
        exist when we make a GET request to the songs/ endpoint
        """
        # hit the API endpoint
        response = self.client.get(
            reverse("phone_contacts-all", kwargs={"version": "v1"})
        )
        # fetch the data from db
        expected = PhoneContact.objects.all()
        serialized = PhoneContactSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
"""