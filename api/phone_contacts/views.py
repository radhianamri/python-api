from django.shortcuts import render, redirect
from .models import PhoneContact
from .serializers import FullPhoneContactSerializer, PhoneContactSerializer, ProfilePhotoSerializer
from .forms import PhoneContactForm

from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser, JSONParser

class PhoneContactView(generics.GenericAPIView):
    def get(self, request, format=None):
        """
            View all phone contact records
        """
        phoneContacts = PhoneContact.objects.all()
        serializer = FullPhoneContactSerializer(phoneContacts, many=True)
        return Response(serializer.data)

@parser_classes((JSONParser, ))
class PhoneContactAdd(generics.GenericAPIView):
    serializer_class = PhoneContactSerializer

    def post(self, request, format=None):
        """
            Add new phone contact record
        """
        serializer = PhoneContactSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status_desc': 'Phone contact record successfully added', 'data': serializer.data}, status=status.HTTP_201_CREATED)

        return Response({'error_desc': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

@parser_classes((JSONParser, ))
class PhoneContactModify(generics.GenericAPIView):
    serializer_class = PhoneContactSerializer
    
    def put(self, request, id, format=None):
        """
            Update phone contact record
        """
        phone_contact = PhoneContact.objects.get(id=id)
        serializer = PhoneContactSerializer(phone_contact, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status_desc': 'Phone contact record successfully updated', 'data': serializer.data}, status=status.HTTP_201_CREATED)

        return Response({'error_desc': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format=None):
        """
            Delete phone contact record
        """
        phone_contact = PhoneContact.objects.get(id=id)
        serializer = PhoneContactSerializer(phone_contact, data=request.data)
        if serializer.is_valid():
            phone_contact.delete()
            return Response({'status_desc': 'Phone contact record successfully deleted', 'data': serializer.data}, status=status.HTTP_201_CREATED)

        return Response({'error_desc': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

@parser_classes((MultiPartParser, ))
class ProfilePhotoUpload(generics.GenericAPIView):
    serializer_class = ProfilePhotoSerializer
    
    def post(self, request, id, format=None):
        """
            Add/Update profile photo
        """
        phone_contact = PhoneContact.objects.get(id=id)
        serializer = ProfilePhotoSerializer(phone_contact, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status_desc': 'Success upload Image', 'data': serializer.data}, status=status.HTTP_201_CREATED)

        return Response({'error_desc': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)