from rest_framework import serializers
from .models import PhoneContact

class FullPhoneContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneContact
        fields = ('id', 'contact_name', 'mobile_number', 'email','profile_photo')
        
class PhoneContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneContact
        fields = ('id', 'contact_name', 'mobile_number', 'email')

class ProfilePhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneContact
        fields = ('id','profile_photo')