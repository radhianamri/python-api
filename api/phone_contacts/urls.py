from django.urls import path, include
from django.conf.urls import url
from .views import PhoneContactView, PhoneContactModify, PhoneContactAdd, ProfilePhotoUpload

urlpatterns = [
    path('phone_contacts', PhoneContactView.as_view(), name='viewPhoneContacts'),
    path('phone_contacts/new', PhoneContactAdd.as_view(), name='addPhoneContacts'),
    path('phone_contacts/<int:id>', PhoneContactModify.as_view(), name='updateDeletePhoneContacts'),
    path('phone_contacts/image/<int:id>/upload', ProfilePhotoUpload.as_view(), name='addProfilePhoto')
]



