from django.apps import AppConfig


class PhoneContactConfig(AppConfig):
    name = 'phone_contacts'
