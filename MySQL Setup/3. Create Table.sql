use finaccel;

CREATE TABLE IF NOT EXISTS phone_contacts(
	contact_id int NOT NULL AUTO_INCREMENT,
	contact_name varchar(30) NOT NULL,
	mobile_number varchar(20) NOT NULL,
	email varchar(30),
	profile_photo varchar(50),
	created_dt DATETIME default NOW(),
	last_updated_dt DATETIME default NOW(),
	PRIMARY KEY (contact_id)
	
) ENGINE=InnoDB;