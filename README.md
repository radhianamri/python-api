# Simple CRUD Python API using Django

This project is to demonstrate how to build simple CRUD API using Django and MySQL DB.

## Getting Started
Before we start, we need this to be installed:
- [Python3](https://www.python.org/downloads/)

Then open up your terminal and run the following commands:
```
pip install django
pip install djangorestframework
pip install django-rest-swagger==2.1.1
```

## Database Setup
Open up MYSQL and start of by creating a fresh new database with the following script:
```
create database finaccel;
```
Then create a new superuser
```
CREATE USER 'finaccel_admin'@'localhost' IDENTIFIED BY '4dm!n';
GRANT ALL PRIVILEGES ON finaccel.* TO 'finaccel_admin'@'localhost';
```

## Project Setup
Next clone the project and go inside folder api
```
cd api
```
Now run teh following in terminal to sync database by creating new tables:
```
python manage.py migrate
```
Next try out to run the code
```
python manage.py runserver
```

## API Documentation
With the server still running, you can open up swagger using the url [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)
The list of APIs provided are:
1. View all phone contacts
2. Insert new phone contact
3. Update existing phone contact
4. Delete existing phone contact
5. Add/update profile photo of existing phone contact

## Notes
For the last API to Add/update profile photo, it can be tested using [Postman](https://www.getpostman.com/downloads) with the following steps:
1. Open up Postman and create a new request
2. Enter url with the following format `http://127.0.0.1:8000/phone_contacts/image/{id}/upload` (id needs to be filled with existing record id)
3. Change method type to `POST` and choose `form-data`
4. Click `Body` enter `profile_photo` within the `key` and choose `File` for the type
5. Click on `Choose files` and choose an image file
6. Click Send

